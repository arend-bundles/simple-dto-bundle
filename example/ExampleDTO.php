<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

use ArendBundles\SimpleDTOBundle\Attributes\PublicField;
use ArendBundles\SimpleDTOBundle\Attributes\StringValue;
use ArendBundles\SimpleDTOBundle\DTO\DTOInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ExampleDTO.
 */
final class ExampleDTO implements DTOInterface
{
    /**
     * @var string
     */
    #[Assert\NotBlank, StringValue('someProperty'), PublicField(true)]
    public string $someProperty;
}