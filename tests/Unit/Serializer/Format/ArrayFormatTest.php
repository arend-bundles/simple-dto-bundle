<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace Test\Unit\Serializer\Format;

use ArendBundles\SimpleDTOBundle\DTO\Factory\DTOFactoryInterface;
use ArendBundles\SimpleDTOBundle\DTO\Factory\PropertyFactoryInterface;
use ArendBundles\SimpleDTOBundle\Serializer\Format\ArrayFormat;
use ArendBundles\SimpleDTOBundle\Serializer\Format\ArrayFormatSerializer;
use ArendBundles\SimpleDTOBundle\Serializer\Format\FormatSerializerInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class ArrayFormatTest.
 */
final class ArrayFormatTest extends TestCase
{
    /**
     *
     */
    public function testGetSerializerClass(): void
    {
        $format = new ArrayFormat(
            new ArrayFormatSerializer(
                $this->createMock(PropertyFactoryInterface::class),
                $this->createMock(DTOFactoryInterface::class)
            )
        );
        self::assertEquals(ArrayFormatSerializer::class, $format->getSerializerClass());

        self::assertNotNull($format->getSerializer());
    }
}