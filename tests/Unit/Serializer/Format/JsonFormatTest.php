<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace Test\Unit\Serializer\Format;

use ArendBundles\SimpleDTOBundle\DTO\Factory\DTOFactoryInterface;
use ArendBundles\SimpleDTOBundle\DTO\Factory\PropertyFactoryInterface;
use ArendBundles\SimpleDTOBundle\Serializer\Format\ArrayFormatSerializer;
use ArendBundles\SimpleDTOBundle\Serializer\Format\JsonFormat;
use ArendBundles\SimpleDTOBundle\Serializer\Format\JsonFormatSerializer;
use PHPUnit\Framework\TestCase;
use RuntimeException;

/**
 * Class JsonFormatTest.
 */
final class JsonFormatTest extends TestCase
{
    /**
     *
     */
    public function testGetSerializerClass(): void
    {
        $arrayFormatSerializer =
            new ArrayFormatSerializer(
                $this->createMock(PropertyFactoryInterface::class),
                $this->createMock(DTOFactoryInterface::class)
            );
        $format = new JsonFormat(
            new JsonFormatSerializer($arrayFormatSerializer)
        );
        self::assertEquals(JsonFormatSerializer::class, $format->getSerializerClass());

        try {
            new JsonFormat($arrayFormatSerializer);
            self::fail('Expected exception');
        } catch (RuntimeException $exception) {
            self::assertNotNull($exception);
        }
    }
}