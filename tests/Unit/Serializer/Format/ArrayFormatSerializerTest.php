<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace Test\Unit\Serializer\Format;

use ArendBundles\SimpleDTOBundle\DTO\DTOInterface;
use ArendBundles\SimpleDTOBundle\DTO\DTOPropertyInterface;
use ArendBundles\SimpleDTOBundle\DTO\Factory\DTOFactoryInterface;
use ArendBundles\SimpleDTOBundle\DTO\Factory\PropertyFactoryInterface;
use ArendBundles\SimpleDTOBundle\DTO\SampleDTO\CompleteDTO;
use ArendBundles\SimpleDTOBundle\Serializer\Format\ArrayFormatSerializer;
use PHPUnit\Framework\TestCase;

/**
 * Class ArrayFormatSerializerTest.
 */
final class ArrayFormatSerializerTest extends TestCase
{
    /**
     *
     */
    public function testDoSerialize(): void
    {
        $mockDTOFactory = $this->createMock(DTOFactoryInterface::class);
        $mockPropertyFactory = $this->createMock(PropertyFactoryInterface::class);
        $serializer = new ArrayFormatSerializer($mockPropertyFactory, $mockDTOFactory);
        $realDTO = new CompleteDTO();
        $reflectionProperty1 = $this->createMock(DTOPropertyInterface::class);
        $reflectionProperty1->expects(self::once())->method('getPropertyName')->willReturn('stringProperty');
        $reflectionProperty1->expects(self::once())->method('get')->willReturn('string_property');
        $reflectionProperty2 = $this->createMock(DTOPropertyInterface::class);
        $reflectionProperty2->expects(self::once())->method('getPropertyName')->willReturn('fake');
        $reflectionProperty2->expects(self::never())->method('get');
        $reflectionProperty3 = $this->createMock(DTOPropertyInterface::class);
        $reflectionProperty3->expects(self::once())->method('getPropertyName')->willReturn('boolProperty');
        $reflectionProperty3->expects(self::once())->method('get')->willReturn('bool_property');
        $reflectionProperty4 = $this->createMock(DTOPropertyInterface::class);
        $reflectionProperty4->expects(self::once())->method('getPropertyName')->willReturn('floatProperty');
        $reflectionProperty4->expects(self::once())->method('get')->willReturn('float_property');
        $mockPropertyFactory->expects(self::exactly(4))
            ->method('create')->willReturnOnConsecutiveCalls(
                $reflectionProperty1,
                $reflectionProperty2,
                $reflectionProperty3,
                $reflectionProperty4,
            );


        $data = $serializer->doSerialize($realDTO);
        self::assertCount(3, $data);
    }

    /**
     *
     */
    public function testToArray(): void
    {
        $mockDTOFactory = $this->createMock(DTOFactoryInterface::class);
        $mockPropertyFactory = $this->createMock(PropertyFactoryInterface::class);
        $serializer = new ArrayFormatSerializer($mockPropertyFactory, $mockDTOFactory);
        $realDTO = new CompleteDTO();
        $reflectionProperty1 = $this->createMock(DTOPropertyInterface::class);
        $reflectionProperty1->expects(self::once())->method('getPropertyName')->willReturn('stringProperty');
        $reflectionProperty1->expects(self::once())->method('get')->willReturn('string_property');
        $reflectionProperty2 = $this->createMock(DTOPropertyInterface::class);
        $reflectionProperty2->expects(self::once())->method('getPropertyName')->willReturn('fake');
        $reflectionProperty2->expects(self::never())->method('get');
        $reflectionProperty3 = $this->createMock(DTOPropertyInterface::class);
        $reflectionProperty3->expects(self::once())->method('getPropertyName')->willReturn('boolProperty');
        $reflectionProperty3->expects(self::once())->method('get')->willReturn('bool_property');
        $reflectionProperty4 = $this->createMock(DTOPropertyInterface::class);
        $reflectionProperty4->expects(self::once())->method('getPropertyName')->willReturn('floatProperty');
        $reflectionProperty4->expects(self::once())->method('get')->willReturn('float_property');
        $mockPropertyFactory->expects(self::exactly(4))
            ->method('create')->willReturnOnConsecutiveCalls(
                $reflectionProperty1,
                $reflectionProperty2,
                $reflectionProperty3,
                $reflectionProperty4,
            );


        $data = $serializer->toArray($realDTO);
        self::assertCount(3, $data);
    }

    /**
     *
     */
    public function testDoDeserialize(): void
    {
        $mockDTOFactory = $this->createMock(DTOFactoryInterface::class);
        $mockPropertyFactory = $this->createMock(PropertyFactoryInterface::class);
        $serializer = new ArrayFormatSerializer($mockPropertyFactory, $mockDTOFactory);
        $data = [
            'intProperty' => 1,
            'floatProperty' => 2,
            'stringProperty' => 'the string',
            'boolProperty' => true,
        ];
        $dto = new CompleteDTO();

        $value = $serializer->doDeserialize($dto, $data);
        self::assertSame($dto, $value);
    }
}