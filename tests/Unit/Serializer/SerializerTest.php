<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace Test\Unit\Serializer;

use ArendBundles\SimpleDTOBundle\DTO\DTOInterface;
use ArendBundles\SimpleDTOBundle\Serializer\Format\FormatSerializerInterface;
use ArendBundles\SimpleDTOBundle\Serializer\Format\SerializerFormat;
use ArendBundles\SimpleDTOBundle\Serializer\Serializer;
use PHPUnit\Framework\TestCase;

/**
 * Class SerializerTest.
 */
final class SerializerTest extends TestCase
{
    /**
     *
     */
    public function testSerialize(): void
    {
        $serializer = new Serializer();
        $mockFormat = $this->createMock(SerializerFormat::class);
        $mockSerializer = $this->createMock(FormatSerializerInterface::class);
        $mockFormat->expects(self::once())->method('getSerializer')->willReturn($mockSerializer);
        $mockSerializer->expects(self::once())->method('doSerialize');
        $mockDTO = $this->createMock(DTOInterface::class);
        $serializer->serialize($mockDTO, $mockFormat);
    }

    /**
     *
     */
    public function testDeserialize(): void
    {
        $serializer = new Serializer();
        $mockFormat = $this->createMock(SerializerFormat::class);
        $mockSerializer = $this->createMock(FormatSerializerInterface::class);
        $mockFormat->expects(self::once())->method('getSerializer')->willReturn($mockSerializer);
        $mockSerializer->expects(self::once())->method('doDeserialize');
        $mockDTO = $this->createMock(DTOInterface::class);
        $serializer->deserialize($mockDTO, $mockFormat, ['data']);
    }

}