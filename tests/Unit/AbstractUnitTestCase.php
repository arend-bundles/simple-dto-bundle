<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace Test\Unit;

use ArendBundles\SimpleDTOBundle\Attributes\PublicField;
use ArendBundles\SimpleDTOBundle\Attributes\StringValue;
use ArendBundles\SimpleDTOBundle\DTO\DTOProperty;
use ArendBundles\SimpleDTOBundle\DTO\DTOPropertyInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class AbstractUnitTest
 */
class AbstractUnitTestCase extends TestCase
{
    public const NAME = 'testProperty';
    public const TYPE = 'int';
    public const STR_VAL = 'test_property';
    public const PUBLIC_FIELD = true;

    /**
     * @param string               $name
     * @param string               $type
     * @param array<string, mixed> $attributes
     *
     * @return DTOProperty|DTOPropertyInterface
     */
    protected function newDTOProperty(string $name = self::NAME, string $type = self::TYPE, array $attributes = [StringValue::NAME => self::STR_VAL, PublicField::NAME => self::PUBLIC_FIELD])
    {
        return new DTOProperty(
            $name,
            $type,
            $attributes,
        );
    }

}