<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace Test\Unit\DTO;

use ArendBundles\SimpleDTOBundle\Attributes\PublicField;
use ArendBundles\SimpleDTOBundle\Attributes\StringValue;
use ArendBundles\SimpleDTOBundle\DTO\DTOFormType;
use ArendBundles\SimpleDTOBundle\DTO\DTOProperty;
use ArendBundles\SimpleDTOBundle\DTO\Factory\DTOFactoryInterface;
use ArendBundles\SimpleDTOBundle\DTO\Factory\ReflectedDTOFactoryInterface;
use ArendBundles\SimpleDTOBundle\DTO\ReflectedDTOInterface;
use ArendBundles\SimpleDTOBundle\DTO\SampleDTO\CompleteDTO;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Test\Unit\AbstractUnitTestCase;

/**
 * Class DTOFormTypeTest.
 */
final class DTOFormTypeTest extends AbstractUnitTestCase
{
    /**
     *
     */
    public function testBuildForm(): void
    {
        $mockReflectedDTOFactory = $this->createMock(ReflectedDTOFactoryInterface::class);
        $mockDTOFactory = $this->createMock(DTOFactoryInterface::class);

        $type = new DTOFormType($mockReflectedDTOFactory, $mockDTOFactory);

        $mockBuilder = $this->createMock(FormBuilderInterface::class);
        $options = ['DTOClassName' => CompleteDTO::class];
        $mockReflectedDTO = $this->createMock(ReflectedDTOInterface::class);
        $dtoProperty = $this->newDTOProperty('testProperty', 'string', [StringValue::NAME => 'testProperty', PublicField::NAME => true]);
        $mockReflectedDTO->expects(self::once())->method('allProperties')->willReturn([$dtoProperty]);
        $mockReflectedDTOFactory->expects(self::once())->method('create')->willReturn($mockReflectedDTO);

        $mockBuilder->expects(self::once())->method('add');
        $type->buildForm($mockBuilder, $options);
    }

    /**
     *
     */
    public function testConfigureOptions(): void
    {
        $mockReflectedDTOFactory = $this->createMock(ReflectedDTOFactoryInterface::class);
        $mockDTOFactory = $this->createMock(DTOFactoryInterface::class);

        $type = new DTOFormType($mockReflectedDTOFactory, $mockDTOFactory);

        $mockResolver = $this->createMock(OptionsResolver::class);
        $mockResolver->expects(self::once())->method('setRequired');
        $type->configureOptions($mockResolver);
    }

    /**
     * @dataProvider typeProvider
     *
     * @param string $type
     * @param string $expected
     */
    public function testMapFormType(string $type, string $expected): void
    {
        $mockReflectedDTOFactory = $this->createMock(ReflectedDTOFactoryInterface::class);
        $mockDTOFactory = $this->createMock(DTOFactoryInterface::class);

        $formType = new DTOFormType($mockReflectedDTOFactory, $mockDTOFactory);

        self::assertEquals($expected, $formType->mapFormType($type));
    }

    /**
     * @return array<array<string, string>>
     */
    public function typeProvider(): array
    {
        return [
            [
                'type' => 'float',
                'expected' => NumberType::class,
            ],
            [
                'type' => 'int',
                'expected' => IntegerType::class,
            ],
            [
                'type' => 'string',
                'expected' => TextType::class,
            ],
            [
                'type' => 'bool',
                'expected' => CheckboxType::class,
            ],
            [
                'type' => 'None of the above',
                'expected' => HiddenType::class,
            ],
        ];
    }
}