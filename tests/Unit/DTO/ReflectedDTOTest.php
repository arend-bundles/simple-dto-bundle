<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace Test\Unit\DTO;

use ArendBundles\SimpleDTOBundle\Attributes\DTOAttribute;
use ArendBundles\SimpleDTOBundle\Attributes\PublicField;
use ArendBundles\SimpleDTOBundle\Attributes\StringValue;
use ArendBundles\SimpleDTOBundle\DTO\DTOProperty;
use ArendBundles\SimpleDTOBundle\DTO\ReflectedDTO;
use ArendBundles\SimpleDTOBundle\DTO\SampleDTO\CompleteDTO;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Test\Unit\AbstractUnitTestCase;

/**
 * Class ReflectedDTOTest.
 */
final class ReflectedDTOTest extends AbstractUnitTestCase
{
    /**
     * @var ReflectedDTO
     */
    private ReflectedDTO $reflectedDTO;

    /**
     * Initializes the reflected DTO with 3 real DTO properties
     */
    public function setUp(): void
    {
        $this->reflectedDTO = new ReflectedDTO([
            'test1' => $this->newDTOProperty('test1', 'string', [StringValue::NAME => 'test', PublicField::NAME => false]),
            'test2' => $this->newDTOProperty('test2', 'string', [StringValue::NAME => 'test2', PublicField::NAME => false]),
            'test3' => $this->newDTOProperty('test3', 'string', [StringValue::NAME => 'test3', PublicField::NAME => true]),
        ],
        CompleteDTO::class
        );
    }

    /**
     *
     */
    public function testHasProperty(): void
    {
        self::assertFalse($this->reflectedDTO->hasProperty('test0'));
        self::assertTrue($this->reflectedDTO->hasProperty('test1'));
        self::assertFalse($this->reflectedDTO->hasProperty('test'));
        self::assertTrue($this->reflectedDTO->hasProperty('test2'));
        self::assertTrue($this->reflectedDTO->hasProperty('test3'));
        
    }

    /**
     *
     */
    public function testGetProperty(): void
    {
        $property = $this->reflectedDTO->getProperty('test1');
        self::assertFalse($property->get(PublicField::NAME));
        self::assertEquals('test', $property->get(StringValue::NAME));

        try {
            $this->reflectedDTO->getProperty('test');
            self::fail('Expected exception');
        } catch (InvalidArgumentException $exception) {
            self::assertEquals('Property: `test` has not been set.', $exception->getMessage());
        }
    }

    /**
     *
     */
    public function testAllProperties(): void
    {
        self::assertCount(3, $this->reflectedDTO->allProperties());
    }
}