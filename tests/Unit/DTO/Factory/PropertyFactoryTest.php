<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace Test\Unit\DTO\Factory;

use ArendBundles\SimpleDTOBundle\DTO\Factory\PropertyFactory;
use ArendBundles\SimpleDTOBundle\DTO\SampleDTO\CompleteDTO;
use PHPUnit\Framework\TestCase;
use ReflectionProperty;

/**
 * Class PropertyFactoryTest.
 */
final class PropertyFactoryTest extends TestCase
{
    /**
     *
     */
    public function testCreate(): void
    {
        $factory = new PropertyFactory();
        $reflection = new \ReflectionClass(CompleteDTO::class);
        $property = $factory->create($reflection->getProperties()[0]);
        self::assertNotNull($property);
    }
}