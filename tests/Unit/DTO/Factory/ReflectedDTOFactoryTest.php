<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace Test\Unit\DTO\Factory;

use ArendBundles\SimpleDTOBundle\DTO\Exception\InvalidClassStringException;
use ArendBundles\SimpleDTOBundle\DTO\Factory\PropertyFactoryInterface;
use ArendBundles\SimpleDTOBundle\DTO\Factory\ReflectedDTOFactory;
use ArendBundles\SimpleDTOBundle\DTO\ReflectedDTOInterface;
use ArendBundles\SimpleDTOBundle\DTO\SampleDTO\CompleteDTO;
use PHPUnit\Framework\TestCase;

/**
 * Class ReflectedDTOFactoryTest.
 */
final class ReflectedDTOFactoryTest extends TestCase
{
    public function testCreate(): void
    {
        $mockPropertyFactory = $this->createMock(PropertyFactoryInterface::class);
        $factory = new ReflectedDTOFactory($mockPropertyFactory);

        try {
            $reflectedDTO = $factory->create(CompleteDTO::class);
            self::assertNotNull($reflectedDTO);
        } catch (InvalidClassStringException $exception) {
            self::fail($exception->getMessage());
        }

        try {
            $factory->create('Some fake class name');
            self::fail('Expected exception');
        } catch (InvalidClassStringException $exception) {
            self::assertNotNull($exception);
        }
    }
}