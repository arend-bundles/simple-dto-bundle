<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace Test\Unit\DTO\Factory;

use ArendBundles\SimpleDTOBundle\DTO\DTOProperty;
use ArendBundles\SimpleDTOBundle\DTO\Exception\ClassIsAbstractException;
use ArendBundles\SimpleDTOBundle\DTO\Exception\InvalidClassStringException;
use ArendBundles\SimpleDTOBundle\DTO\Exception\InvalidConstructorException;
use ArendBundles\SimpleDTOBundle\DTO\Exception\MissingInterfaceException;
use ArendBundles\SimpleDTOBundle\DTO\Factory\DefaultDTOFactory;
use ArendBundles\SimpleDTOBundle\DTO\SampleDTO\AbstractDTO;
use ArendBundles\SimpleDTOBundle\DTO\SampleDTO\CompleteDTO;
use ArendBundles\SimpleDTOBundle\DTO\SampleDTO\InvalidDTO;
use PHPUnit\Framework\TestCase;


/**
 * Class DefaultDTOFactoryTest.
 */
final class DefaultDTOFactoryTest extends TestCase
{
    /**
     * @dataProvider classNameProvider
     *
     * @param string $className
     * @param string $expected
     */
    public function testCreate(string $className, string $expected): void
    {
        $result = null;
        $factory = new DefaultDTOFactory();
        try {
            $result = $factory->create($className);
        } catch (ClassIsAbstractException
        | InvalidClassStringException
        | MissingInterfaceException
        | InvalidConstructorException $exception) {
            $result = $exception;
        }
        self::assertEquals($expected, \get_class($result));
    }

    /**
     * @return array<array<string, string>>
     */
    public function classNameProvider(): array
    {
        return [
            [
                'className' => CompleteDTO::class,
                'expected' => CompleteDTO::class,
            ],
            [
                'className' => AbstractDTO::class,
                'expected' => ClassIsAbstractException::class,
            ],
            [
                'className' => InvalidDTO::class,
                'expected' => InvalidConstructorException::class,
            ],
            [
                'className' => DTOProperty::class,
                'expected' => MissingInterfaceException::class,
            ],
            [
                'className' => 'Non-existent class',
                'expected' => InvalidClassStringException::class,
            ],
        ];
    }
}