<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace Test\Unit\DTO;

use ArendBundles\SimpleDTOBundle\Attributes\PublicField;
use ArendBundles\SimpleDTOBundle\Attributes\StringValue;
use ArendBundles\SimpleDTOBundle\DTO\DTOProperty;
use PHPUnit\Framework\TestCase;
use Test\Unit\AbstractUnitTestCase;

/**
 * Class DTOPropertyTest.
 */
final class DTOPropertyTest extends AbstractUnitTestCase
{
    /**
     * @var DTOProperty
     */
    private DTOProperty $property;

    /**
     * Initializes the property;
     */
    public function setUp(): void
    {
        $this->property = $this->newDTOProperty();
    }

    /**
     *
     */
    public function testGetPropertyName(): void
    {
        self::assertEquals(self::NAME, $this->property->getPropertyName());
    }

    /**
     *
     */
    public function testGetPropertyType(): void
    {
        self::assertEquals(self::TYPE, $this->property->getPropertyType());
    }

    public function testGet(): void
    {
        self::assertEquals(self::STR_VAL, $this->property->get(StringValue::NAME));
        self::assertEquals(self::PUBLIC_FIELD, $this->property->get(PublicField::NAME));
    }
}