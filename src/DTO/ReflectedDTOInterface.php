<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO;

use JetBrains\PhpStorm\Pure;

/**
 * Interface ReflectedDTOInterface
 */
interface ReflectedDTOInterface
{
    /**
     * @param string $propertyName
     *
     * @return bool
     */
    #[Pure]
    public function hasProperty(string $propertyName): bool;

    /**
     * @param string $propertyName
     *
     * @return DTOPropertyInterface
     */
    public function getProperty(string $propertyName): DTOPropertyInterface;

    /**
     * @return array<DTOPropertyInterface>
     */
    public function allProperties(): array;

    /**
     * @return DTOPropertyInterface
     */
    public function getClassProperties(): DTOPropertyInterface;
}