<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO;

use ArendBundles\SimpleDTOBundle\DTO\Exception\MissingAttributeException;

/**
 * Class ClassAttributes
 */
class ClassAttributes implements DTOPropertyInterface
{
    /**
     * @var class-string
     */
    private string $className;

    /**
     * @var array<string, mixed>
     */
    private array $attributeValues;

    /**
     * ClassAttributes constructor.
     *
     * @param class-string $className
     * @param array<string, mixed>  $attributeValues
     */
    public function __construct(string $className, array $attributeValues)
    {
        $this->className = $className;
        $this->attributeValues = $attributeValues;
    }

    /**
     * @param string $id
     *
     * @throws MissingAttributeException
     *
     * @return mixed
     */
    public function get(string $id): mixed
    {
        if (!$this->has($id)) {
            throw new MissingAttributeException(sprintf('Failed to get value for attribute: `%s`. It must be set on the whole DTO class.', $id));
        }

        return $this->attributeValues[$id];
    }

    /**
     * @param string $id
     *
     * @return bool
     */
    public function has(string $id): bool
    {
        return array_key_exists($id, $this->attributeValues);
    }

    /**
     * @return class-string
     */
    public function getPropertyName(): string
    {
        return $this->className;
    }

    /**
     * @return class-string
     */
    public function getPropertyType(): string
    {
        return DTOPropertyInterface::class;
    }
}