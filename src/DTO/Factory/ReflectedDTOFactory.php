<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO\Factory;

use ArendBundles\SimpleDTOBundle\DTO\DTOInterface;
use ArendBundles\SimpleDTOBundle\DTO\Exception\InvalidClassStringException;
use ArendBundles\SimpleDTOBundle\DTO\ReflectedDTO;
use ArendBundles\SimpleDTOBundle\DTO\ReflectedDTOInterface;
use ReflectionClass;
use ReflectionException;

/**
 * Class DTOReflector.
 */
final class ReflectedDTOFactory implements ReflectedDTOFactoryInterface
{
    /**
     * @var PropertyFactoryInterface
     */
    private PropertyFactoryInterface $propertyFactory;

    /**
     * DTOReflector constructor.
     *
     * @param PropertyFactoryInterface $propertyFactory
     */
    public function __construct(PropertyFactoryInterface $propertyFactory)
    {
        $this->propertyFactory = $propertyFactory;
    }

    /**
     * {@inheritDoc}
     */
    public function create(DTOInterface|string $dto): ReflectedDTOInterface
    {
        try {
            $reflection = new ReflectionClass($dto);
        } catch (ReflectionException) {
            throw new InvalidClassStringException($dto);
        }

        $reflectionSet = [];

        /** @var class-string $dtoClassName */
        $dtoClassName = $dto;

        if ($dto instanceof DTOInterface) {
            $dtoClassName = \get_class($dto);
        }

        $reflectionSet[$dtoClassName] = $this->propertyFactory->createClassAttributes($reflection);

        $properties = $reflection->getProperties();
        foreach ($properties as $property) {
            $reflectionSet[$property->getName()] = $this->propertyFactory->create($property);
        }

        return new ReflectedDTO($reflectionSet, $dtoClassName);
    }
}