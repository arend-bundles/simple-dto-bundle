<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO\Factory;


use ArendBundles\SimpleDTOBundle\DTO\DTOPropertyInterface;
use ReflectionClass;
use ReflectionObject;
use ReflectionProperty;

/**
 * Interface PropertyFactoryInterface
 * @package ArendBundles\SimpleDTOBundle\DTO\Factory
 */
interface PropertyFactoryInterface
{
    /**
     * @param ReflectionProperty $property
     *
     * @return DTOPropertyInterface
     */
    public function create(ReflectionProperty $property): DTOPropertyInterface;

    /**
     * @param ReflectionClass<object> $reflectionClass
     *
     * @return DTOPropertyInterface
     */
    public function createClassAttributes(ReflectionClass $reflectionClass): DTOPropertyInterface;
}