<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO\Factory;

use ArendBundles\SimpleDTOBundle\DTO\DTOInterface;
use ArendBundles\SimpleDTOBundle\DTO\Exception\ClassIsAbstractException;
use ArendBundles\SimpleDTOBundle\DTO\Exception\InvalidClassStringException;
use ArendBundles\SimpleDTOBundle\DTO\Exception\InvalidConstructorException;
use ArendBundles\SimpleDTOBundle\DTO\Exception\MissingInterfaceException;
use ReflectionClass;
use ReflectionException;
use RuntimeException;

/**
 * Class DefaultDTOFactory
 */
class DefaultDTOFactory implements DTOFactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function create(string $className): DTOInterface
    {
        try {
            $reflection = new ReflectionClass($className);
        } catch (ReflectionException) {
            throw new InvalidClassStringException($className);
        }

        $interfaces = class_implements($className, true);
        if ($interfaces && !array_key_exists(DTOInterface::class, $interfaces))  {
            throw new MissingInterfaceException($className);
        }

        if ($reflection->isAbstract()) {
            throw new ClassIsAbstractException($className);
        }

        $constructor = $reflection->getConstructor();

        if (null !== $constructor && $constructor->getNumberOfParameters() > 0) {
            throw new InvalidConstructorException($constructor, $className);
        }

        return new $className();
    }
}