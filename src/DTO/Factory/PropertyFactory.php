<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO\Factory;

use ArendBundles\SimpleDTOBundle\Attributes\PublicField;
use ArendBundles\SimpleDTOBundle\Attributes\StringValue;
use ArendBundles\SimpleDTOBundle\DTO\ClassAttributes;
use ArendBundles\SimpleDTOBundle\DTO\DTOInterface;
use ArendBundles\SimpleDTOBundle\DTO\DTOProperty;
use ArendBundles\SimpleDTOBundle\DTO\DTOPropertyInterface;
use JetBrains\PhpStorm\Pure;
use LogicException;
use ReflectionClass;
use ReflectionProperty;

/**
 * Class PropertyFactory.
 */
class PropertyFactory implements PropertyFactoryInterface
{
    /**
     * @param ReflectionProperty $property
     *
     * @return DTOPropertyInterface
     */
    public function create(ReflectionProperty $property): DTOPropertyInterface
    {
        $attrs = $property->getAttributes();
        $namedAttrs = [];
        foreach ($attrs as $attr) {
            $args = $attr->getArguments();
            if (\count($args) === 1) {
                $namedAttrs[$attr->getName()] = $args[0];
            }
        }

        $type = (string) ($property->getType() ?? 'mixed');

        $class = $property->class;
        if (! (new $class() instanceof DTOInterface)) {
            throw new LogicException(sprintf('The class of the reflection is said to be: %s, which does not implement DTOInterface', $class));
        }

        return new DTOProperty(
            $property->name,
            $type,
            $namedAttrs
        );
    }

    /**
     * {@inheritDoc}
     */
    public function createClassAttributes(ReflectionClass $reflectionClass): DTOPropertyInterface
    {
        $classAttrs = [];
        $attrs = $reflectionClass->getAttributes();
        foreach ($attrs as $attr) {
            $classAttrs[$attr->getName()] = $attr->getArguments();
        }

        return new ClassAttributes($reflectionClass->name, $classAttrs);
    }
}