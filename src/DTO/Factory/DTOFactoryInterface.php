<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO\Factory;

use ArendBundles\SimpleDTOBundle\DTO\DTOInterface;
use ArendBundles\SimpleDTOBundle\DTO\Exception\ClassIsAbstractException;
use ArendBundles\SimpleDTOBundle\DTO\Exception\InvalidClassStringException;
use ArendBundles\SimpleDTOBundle\DTO\Exception\InvalidConstructorException;
use ArendBundles\SimpleDTOBundle\DTO\Exception\MissingInterfaceException;

/**
 * Interface DTOFactoryInterface.
 */
interface DTOFactoryInterface
{
    /**
     * @param class-string $className
     *
     * @throws InvalidClassStringException
     * @throws InvalidConstructorException
     * @throws ClassIsAbstractException
     * @throws MissingInterfaceException
     *
     * @return DTOInterface
     */
    public function create(string $className): DTOInterface;
}