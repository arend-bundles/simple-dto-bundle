<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO\Factory;

use ArendBundles\SimpleDTOBundle\DTO\DTOInterface;
use ArendBundles\SimpleDTOBundle\DTO\Exception\InvalidClassStringException;
use ArendBundles\SimpleDTOBundle\DTO\ReflectedDTO;
use ArendBundles\SimpleDTOBundle\DTO\ReflectedDTOInterface;

/**
 * Interface ReflectedDTOFactoryInterface
 */
interface ReflectedDTOFactoryInterface
{
    /**
     * @param DTOInterface|class-string $dto
     *
     * @throws InvalidClassStringException
     *
     * @return ReflectedDTOInterface
     */
    public function create(DTOInterface|string $dto): ReflectedDTOInterface;
}