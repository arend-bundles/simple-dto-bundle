<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO;

use ArendBundles\SimpleDTOBundle\DTO\Factory\DefaultDTOFactory;
use ArendBundles\SimpleDTOBundle\DTO\Exception\ClassIsAbstractException;
use ArendBundles\SimpleDTOBundle\DTO\Exception\InvalidClassStringException;
use ArendBundles\SimpleDTOBundle\DTO\Exception\InvalidConstructorException;
use ArendBundles\SimpleDTOBundle\DTO\Exception\MissingInterfaceException;
use ArendBundles\SimpleDTOBundle\DTO\Factory\DTOFactoryInterface;
use ArendBundles\SimpleDTOBundle\DTO\Factory\ReflectedDTOFactoryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DTOFormType.
 */
class DTOFormType extends AbstractType
{
    /**
     * @var DTOFactoryInterface
     */
    private DTOFactoryInterface $defaultDTOFactory;

    /**
     * @var ReflectedDTOFactoryInterface
     */
    private ReflectedDTOFactoryInterface $reflectedDTOFactory;

    /**
     * DTOFormType constructor.
     *
     * @param ReflectedDTOFactoryInterface $reflectedDTOFactory
     * @param DTOFactoryInterface          $defaultDTOFactory
     */
    public function __construct(ReflectedDTOFactoryInterface $reflectedDTOFactory, DTOFactoryInterface $defaultDTOFactory)
    {
        $this->defaultDTOFactory = $defaultDTOFactory;
        $this->reflectedDTOFactory = $reflectedDTOFactory;
    }

    /**
     * @param FormBuilderInterface<mixed> $builder
     * @param array<string, mixed> $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $dtoClassName = $options['DTOClassName'];
        $dto = $this->defaultDTOFactory->create($dtoClassName);
        $reflectedDTO = $this->reflectedDTOFactory->create($dto);
        foreach ($reflectedDTO->allProperties() as $property) {
            $builder->add($property->get('stringValue'), $this->mapFormType($property->getPropertyType()));
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired([
            'DTOClassName'
        ]);
    }

    /**
     * @param string $type
     *
     * @return class-string
     */
    public function mapFormType(string $type): string
    {
        return match ($type) {
            'int' => IntegerType::class,
            'float' => NumberType::class,
            'bool' => CheckboxType::class,
            'string' => TextType::class,
            default => HiddenType::class,
        };
    }

}