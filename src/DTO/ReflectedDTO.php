<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO;

use InvalidArgumentException;
use JetBrains\PhpStorm\Pure;

/**
 * Class ReflectedDTO.
 */
final class ReflectedDTO implements ReflectedDTOInterface
{
    /**
     * @var array<string, DTOPropertyInterface>
     */
    private array $reflectedProperties;

    /**
     * @var class-string
     */
    private string $className;

    /**
     * ReflectedDTO constructor.
     *
     * @param array<string, DTOPropertyInterface> $properties
     * @param class-string $className
     */
    public function __construct(array $properties, string $className)
    {
        foreach ($properties as $key => $property) {
            if ($key !== $property->getPropertyName()) {
                $this->reflectedProperties[$property->getPropertyName()] = $property;
                continue;
            }
            $this->reflectedProperties[$key] = $property;
        }
        $this->className = $className;
    }

    /**
     * @param string $propertyName
     *
     * @return bool
     */
    #[Pure]
    public function hasProperty(string $propertyName): bool
    {
        return array_key_exists($propertyName, $this->reflectedProperties);
    }

    /**
     * {@inheritDoc}
     */
    public function getProperty(string $propertyName): DTOPropertyInterface
    {
        if (!$this->hasProperty($propertyName)) {
            throw new InvalidArgumentException(sprintf('Property: `%s` has not been set.', $propertyName));
        }
        return $this->reflectedProperties[$propertyName];
    }

    /**
     * {@inheritDoc}
     */
    public function allProperties(): array
    {
        return $this->reflectedProperties;
    }

    /**
     * {@inheritDoc}
     */
    public function getClassProperties(): DTOPropertyInterface
    {
        return $this->getProperty($this->getClassName());
    }

    /**
     * @return class-string
     */
    public function getClassName(): string
    {
        return $this->className;
    }
}