<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO;

use Psr\Container\ContainerInterface;

/**
 * Interface DTOPropertyInterface
 */
interface DTOPropertyInterface extends ContainerInterface
{
    /**
     * @return string
     */
    public function getPropertyName(): string;

    /**
     * @return string
     */
    public function getPropertyType(): string;
}