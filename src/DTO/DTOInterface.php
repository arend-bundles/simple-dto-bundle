<?php
declare(strict_types=1);

namespace ArendBundles\SimpleDTOBundle\DTO;

/**
 * Root interface of the simple-dto-bundle, any DTO should implement this interface.
 * The purpose of this interface is simply to mark a class as a DTO, so strict typing is possible.`
 */
interface DTOInterface
{
}