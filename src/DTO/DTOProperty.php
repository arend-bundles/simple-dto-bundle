<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO;

use ArendBundles\SimpleDTOBundle\DTO\Exception\MissingAttributeException;

/**
 * Class DTOProperty.
 */
final class DTOProperty implements DTOPropertyInterface
{
    private string $propertyName;
    private string $propertyType;
    /**
     * @var array<string, mixed>
     */
    private array $attributeValues;

    /**
     * DTOProperty constructor.
     *
     * @param string               $propertyName
     * @param string               $propertyType
     * @param array<string, mixed> $attributeValues
     */
    public function __construct(
        string $propertyName,
        string $propertyType,
        array $attributeValues = []
    ) {
        $propertyType = str_replace('?', '', $propertyType);
        $this->propertyName = $propertyName;
        $this->propertyType = $propertyType;
        $this->attributeValues = $attributeValues;
    }

    /**
     * @return string
     */
    public function getPropertyName(): string
    {
        return $this->propertyName;
    }

    /**
     * @return string
     */
    public function getPropertyType(): string
    {
        return $this->propertyType;
    }

    /**
     * @param string $id
     *
     * @throws MissingAttributeException
     *
     * @return mixed
     */
    public function get(string $id): mixed
    {
        if (!$this->has($id)) {
            throw new MissingAttributeException(sprintf('Failed to get value for attribute: `%s`. It is not set for property: `%s`', $id, $this->propertyName));
        }

        return $this->attributeValues[$id];
    }

    /**
     * @param string $id
     *
     * @return bool
     */
    public function has(string $id): bool
    {
        return array_key_exists($id, $this->attributeValues);
    }
}