<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO\Exception;

/**
 * Class MissingInterfaceException
 */
class MissingInterfaceException extends DTOException
{
    /**
     * MissingInterfaceException constructor.
     *
     * @param class-string $className
     */
    public function __construct(string $className)
    {
        parent::__construct(sprintf('`%s` does not implement DTOInterface, cannot instantiate it.', $className), self::MISSING_INTERFACE);
    }
}