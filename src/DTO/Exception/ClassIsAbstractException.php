<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO\Exception;

/**
 * Class ClassIsAbstractException
 */
class ClassIsAbstractException extends DTOException
{
    /**
     * ClassIsAbstractException constructor.
     *
     * @param string $className
     */
    public function __construct(string $className)
    {
        parent::__construct(sprintf('Cannot instantiate `%s`, because it is abstract', $className), self::CLASS_IS_ABSTRACT);
    }
}