<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO\Exception;

use ReflectionMethod;

/**
 * Class InvalidConstructorException
 */
class InvalidConstructorException extends DTOException
{
    /**
     * @var ReflectionMethod
     */
    private ReflectionMethod $constructor;

    /**
     * InvalidConstructorException constructor.
     *
     * @param ReflectionMethod $constructor
     * @param string           $className
     */
    public function __construct(ReflectionMethod $constructor, string $className)
    {
        parent::__construct(sprintf(
            '`%s` has an invalid constructor, cannot instantiate it. Unexpected `%d` constructor parameters',
            $className,
            $constructor->getNumberOfParameters(),
        ), self::INVALIDCONSTRUCTOR);
        $this->constructor = $constructor;
    }

    /**
     * @return ReflectionMethod
     */
    public function getConstructor(): ReflectionMethod
    {
        return $this->constructor;
    }
}