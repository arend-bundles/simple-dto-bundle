<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO\Exception;

use ArendBundles\SimpleDTOBundle\DTO\DTOInterface;

/**
 * Class InvalidClassStringException
 */
class InvalidClassStringException extends DTOException
{
    /**
     * InvalidClassStringException constructor.
     *
     * @param DTOInterface|class-string $className
     */
    public function __construct(DTOInterface|string $className)
    {
        if (!is_string($className)) {
            $className = \get_class($className);
        }
        $message = sprintf('className: `%s` is not a valid class to construct as a DTO', $className);
        parent::__construct($message, self::INVALID_CLASS_STRING);
    }
}