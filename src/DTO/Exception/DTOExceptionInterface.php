<?php
declare(strict_types=1);

/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO\Exception;

/**
 * Interface DTOExceptionInterface
 * @package ArendBundles\SimpleDTOBundle\DTO\Exception
 */
interface DTOExceptionInterface
{
    public const INVALID_CLASS_STRING = 100;
    public const MISSING_INTERFACE = 200;
    public const CLASS_IS_ABSTRACT = 300;
    public const INVALIDCONSTRUCTOR = 400;

}