<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO\Exception;

use Psr\Container\NotFoundExceptionInterface;

/**
 * Class MissingAttributeException
 */
class MissingAttributeException extends DTOException implements NotFoundExceptionInterface
{
}