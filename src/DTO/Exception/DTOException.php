<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO\Exception;

use Exception;

/**
 * Class DTOException
 */
class DTOException extends Exception implements DTOExceptionInterface
{
}