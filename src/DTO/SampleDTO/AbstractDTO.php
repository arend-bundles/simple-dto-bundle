<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO\SampleDTO;

use ArendBundles\SimpleDTOBundle\Attributes\PublicField;
use ArendBundles\SimpleDTOBundle\Attributes\StringValue;
use ArendBundles\SimpleDTOBundle\DTO\DTOInterface;

/**
 * @internal
 *
 * Class AbstractDTO.
 */
abstract class AbstractDTO implements DTOInterface
{
    /**
     * @var string|null
     */
    #[StringValue('base'), PublicField(false)]
    private ?string $baseProperty;
}