<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO\SampleDTO;

use ArendBundles\SimpleDTOBundle\Attributes\PublicField;
use ArendBundles\SimpleDTOBundle\Attributes\StringValue;
use ArendBundles\SimpleDTOBundle\DTO\DTOInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @internal This class exists for internal reasons. Please do not re-use this.
 *
 * Class CompleteDTO.
 */
final class CompleteDTO extends AbstractDTO
{
    #[PublicField(true), StringValue('string_property'), Assert\NotBlank, Assert\Type('string'), Assert\NotNull]
    public ?string $stringProperty = null;

    #[PublicField(true), StringValue('float_property'), Assert\Type('float'), Assert\NotNull]
    public ?float $floatProperty = null;

    #[PublicField(true), StringValue('int_property'), Assert\Type('int'), Assert\NotNull]
    public ?int $intProperty = null;

    #[PublicField(true), StringValue('bool_property'), Assert\Type('bool'), Assert\NotNull]
    public ?bool $boolProperty = null;
}