<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DTO\SampleDTO;

/**
 * Class InvalidDTO.
 */
final class InvalidDTO extends AbstractDTO
{
    private string $testProperty;

    /**
     * InvalidDTO constructor.
     *
     * @param string $testProperty
     */
    public function __construct(string $testProperty)
    {
        $this->testProperty = $testProperty;
    }
}