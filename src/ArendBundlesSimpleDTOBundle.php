<?php
declare(strict_types=1);

namespace ArendBundles\SimpleDTOBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ArendBundlesSimpleDTOBundle
 */
class ArendBundlesSimpleDTOBundle extends Bundle
{
}