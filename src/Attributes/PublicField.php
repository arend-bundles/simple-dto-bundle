<?php

declare(strict_types=1);

/*
 * @link      https://gitlab.adivare.nl/simple-dto-bundle/simple-dto-bundle
 * @copyright Copyright (c) Adivare BV.
 * @license   Proprietary (see LICENSE for details)
 * @author    See AUTHORS.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ArendBundles\SimpleDTOBundle\Attributes;
use Attribute;

/**
 * Class PublicField.
 */
#[Attribute]
final class PublicField implements DTOAttribute
{
    /** @var string NAME */
    public const NAME = 'publicField';

    /**
     * @var bool
     */
    public bool $public;

    public function __construct(bool $public)
    {
        $this->public = $public;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
