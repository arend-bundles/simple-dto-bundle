<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\Attributes;
use ArendBundles\SimpleDTOBundle\DTO\Exception\InvalidClassStringException;
use Attribute;

/**
 * Class EntityClass
 */
#[Attribute]
final class EntityClass implements DTOAttribute
{
    public const NAME = 'entityClass';

    /**
     * @var string
     */
    public string $entityClassName;

    /**
     * EntityClass constructor.
     *
     * @param class-string $entityClassName
     *
     * @throws InvalidClassStringException
     */
    public function __construct(string $entityClassName)
    {
        if (!class_exists($entityClassName)) {
            throw new InvalidClassStringException($entityClassName);
        }
        $this->entityClassName = $entityClassName;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}