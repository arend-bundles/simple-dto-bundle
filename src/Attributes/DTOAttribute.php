<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\Attributes;


interface DTOAttribute
{
    /**
     * This represents the id of the attribute, by which it can be referenced.
     *
     * @return string
     */
    public function getName(): string;
}