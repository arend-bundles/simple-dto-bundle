<?php

declare(strict_types=1);

/*
 * @link      https://gitlab.adivare.nl/simple-dto-bundle/simple-dto-bundle
 * @copyright Copyright (c) Adivare BV.
 * @license   Proprietary (see LICENSE for details)
 * @author    See AUTHORS.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ArendBundles\SimpleDTOBundle\Attributes;
use Attribute;

/**
 * Class StringValue.
 */
#[Attribute]
final class StringValue implements DTOAttribute
{
    /** @var string NAME */
    public const NAME = 'stringValue';

    /**
     * @var string
     */
    public string $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
