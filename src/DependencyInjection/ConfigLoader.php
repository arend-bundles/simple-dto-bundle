<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;

/**
 * Class ConfigLoader.
 */
final class ConfigLoader
{
    /**
     * @param ContainerBuilder $builder
     *
     * @return YamlFileLoader
     */
    public static function getYamlLoader(ContainerBuilder $builder): YamlFileLoader
    {
        return new YamlFileLoader(
            $builder,
            new FileLocator(__DIR__.'/../Resources/config')
        );
    }

}