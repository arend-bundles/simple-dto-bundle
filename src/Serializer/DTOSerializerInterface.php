<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\Serializer;

use ArendBundles\SimpleDTOBundle\DTO\DTOInterface;
use ArendBundles\SimpleDTOBundle\Serializer\Format\SerializerFormat;

/**
 * Interface SerializerService
 */
interface DTOSerializerInterface
{
    /**
     * @param DTOInterface     $dto
     * @param SerializerFormat $format
     *
     * @return mixed
     */
    public function serialize(DTOInterface $dto, SerializerFormat $format);

    /**
     * @param DTOInterface|string $dto
     * @param SerializerFormat    $format
     * @param mixed               $data
     *
     * @return DTOInterface
     */
    public function deserialize(DTOInterface|string $dto, SerializerFormat $format, $data): DTOInterface;
}