<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\Serializer\Format;

/**
 * Class SerializerFormat.
 */
interface SerializerFormat
{
    public const FORMAT_ARRAY = 'format_array';
    public const FORMAT_JSON = 'format_json';

    /**
     * @return FormatSerializerInterface
     */
    public function getSerializer(): FormatSerializerInterface;
}