<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\Serializer\Format;

use ArendBundles\SimpleDTOBundle\DTO\DTOInterface;

/**
 * Interface SerializerInterface
 */
interface FormatSerializerInterface
{
    /**
     * @param DTOInterface $dto
     *
     * @return mixed
     */
    public function doSerialize(DTOInterface $dto);

    /**
     * @param DTOInterface|string $dto
     * @param mixed               $serializedData
     *
     * @return DTOInterface
     */
    public function doDeserialize(DTOInterface|string $dto, $serializedData): DTOInterface;
}