<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\Serializer\Format;

use ArendBundles\SimpleDTOBundle\DTO\DTOInterface;
use ArendBundles\SimpleDTOBundle\DTO\Exception\InvalidClassStringException;
use ArendBundles\SimpleDTOBundle\DTO\Factory\ReflectedDTOFactoryInterface;
use InvalidArgumentException;
use LogicException;
use ReflectionClass;
use ReflectionException;
use RuntimeException;

/**
 * Class EntityFormatSerializer
 */
class EntityFormatSerializer implements FormatSerializerInterface
{
    private ReflectedDTOFactoryInterface $reflectedDTOFactory;

    /**
     * EntityFormatSerializer constructor.
     *
     * @param ReflectedDTOFactoryInterface $reflectedDTOFactory
     */
    public function __construct(ReflectedDTOFactoryInterface $reflectedDTOFactory)
    {
        $this->reflectedDTOFactory = $reflectedDTOFactory;
    }

    /**
     * @param DTOInterface $dto
     *
     * @throws InvalidClassStringException
     *
     * @return object
     */
    public function doSerialize(DTOInterface $dto): object
    {
        $reflectedDTO = $this->reflectedDTOFactory->create($dto);
        $classProperties = $reflectedDTO->getClassProperties();
        $entity = $this->createEntity($dto, $classProperties->get('entityClass'));
        // Adding support for calling setters after initialization will be added later.

        return $entity;
    }

    /**
     * @param DTOInterface|string $dto
     * @param object               $serializedData
     *
     * @return DTOInterface
     */
    public function doDeserialize(DTOInterface|string $dto, $serializedData): DTOInterface
    {
        if (!is_object($serializedData)) {
            throw new InvalidArgumentException('Entity Serializer can only de-serialize entities');
        }
        throw new RuntimeException('Not yet implemented');
    }

    /**
     * @param DTOInterface $dto
     * @param class-string       $entityClass
     *
     * @throws InvalidClassStringException
     *
     * @return object
     */
    private function createEntity(DTOInterface $dto, string $entityClass): object
    {
        try {
            $reflection = new ReflectionClass($entityClass);
        } catch (ReflectionException) {
            throw new InvalidClassStringException($entityClass);
        }
        $constructor = $reflection->getConstructor();
        if (is_null($constructor)) {
            return new $entityClass();
        }
        $parameterMapping = [];
        $mappedProperties = [];
        $properties = $reflection->getProperties();
        foreach ($properties as $property) {
            $mappedProperties[$property->getName()] = $property;
        }
        foreach ($constructor->getParameters() as $parameter) {
            if (false === isset($mappedProperties[$parameter->getName()]) || $parameter->getType() !== $mappedProperties[$parameter->getName()]->getType()) {
                throw new LogicException('DTO properties do not match the parameters of the entity constructor. Support for this will be implemented later');
            }
            $parameterMapping[$parameter->getPosition()] = $dto->$parameter->getName();
        }

        return new $entityClass(...$parameterMapping);
    }
}