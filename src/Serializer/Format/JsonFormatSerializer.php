<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\Serializer\Format;

use ArendBundles\SimpleDTOBundle\DTO\DTOInterface;
use JsonException;

/**
 * Class JsonSerializer.
 */
final class JsonFormatSerializer implements FormatSerializerInterface
{
    /**
     * @var ArraySerializerInterface
     */
    private ArraySerializerInterface $arraySerializer;

    /**
     * JsonSerializer constructor.
     *
     * @param ArraySerializerInterface $arraySerializer
     */
    public function __construct(ArraySerializerInterface $arraySerializer)
    {
        $this->arraySerializer = $arraySerializer;
    }

    /**
     * @param DTOInterface $dto
     *
     * @return string
     * @throws JsonException
     */
    public function doSerialize(DTOInterface $dto): string
    {
        return json_encode($this->arraySerializer->toArray($dto), JSON_THROW_ON_ERROR);
    }

    /**
     * @param DTOInterface|class-string $dto
     * @param string                    $data
     *
     * @return DTOInterface
     * @throws JsonException
     */
    public function doDeserialize(DTOInterface|string $dto, $data): DTOInterface
    {
        return $this->arraySerializer->fromArray(
            $dto,
            json_decode($data, true, 512, JSON_THROW_ON_ERROR)
        );
    }

}