<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\Serializer\Format;

use ArendBundles\SimpleDTOBundle\DTO\DTOInterface;

/**
 * Interface ArraySerializerInterface
 */
interface ArraySerializerInterface
{
    /**
     * @param DTOInterface $dto
     *
     * @return array<string, mixed>
     */
    public function toArray(DTOInterface $dto): array;

    /**
     * @param DTOInterface|string  $dto
     * @param array<string, mixed> $data
     *
     * @return DTOInterface
     */
    public function fromArray(DTOInterface|string $dto, array $data): DTOInterface;

}