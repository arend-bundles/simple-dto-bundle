<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\Serializer\Format;

use LogicException;
use Psr\Container\NotFoundExceptionInterface;

/**
 * Class UnknownFormatException
 */
class UnknownFormatException extends LogicException implements NotFoundExceptionInterface
{
}