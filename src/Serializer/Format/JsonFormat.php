<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\Serializer\Format;

/**
 * Class JsonFormat.
 */
final class JsonFormat extends AbstractFormat
{
    /**
     * JsonFormat constructor.
     *
     * @param FormatSerializerInterface $serializer
     */
    public function __construct(FormatSerializerInterface $serializer)
    {
        $this->setSerializer($serializer);
    }

    /**
     * @return class-string
     */
    public function getSerializerClass(): string
    {
        return JsonFormatSerializer::class;
    }
}