<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\Serializer\Format;

use RuntimeException;

/**
 * Class AbstractFormat.
 */
abstract class AbstractFormat implements SerializerFormat
{
    /**
     * @var FormatSerializerInterface
     */
    protected FormatSerializerInterface $serializer;

    /**
     * @return FormatSerializerInterface
     */
    public function getSerializer(): FormatSerializerInterface
    {
        return $this->serializer;
    }

    /**
     * @return class-string
     */
    abstract public function getSerializerClass(): string;

    /**
     * @param FormatSerializerInterface $serializer
     */
    public function setSerializer(FormatSerializerInterface $serializer): void
    {
        $expectedClass= $this->getSerializerClass();
        $actualClass = \get_class($serializer);
        if ($expectedClass !== $actualClass) {
            throw new RuntimeException(
                sprintf(
                    'Configuration error! Expected serializer of class: `%s`. Provided class: `%s`. Check service definition',
                    $expectedClass,
                    $actualClass,
                )
            );
        }
        $this->serializer = $serializer;
    }
}