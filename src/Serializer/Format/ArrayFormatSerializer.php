<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\Serializer\Format;

use ArendBundles\SimpleDTOBundle\Attributes\StringValue;
use ArendBundles\SimpleDTOBundle\DTO\DTOInterface;
use ArendBundles\SimpleDTOBundle\DTO\Exception\ClassIsAbstractException;
use ArendBundles\SimpleDTOBundle\DTO\Factory\DTOFactoryInterface;
use ArendBundles\SimpleDTOBundle\DTO\Factory\PropertyFactoryInterface;
use InvalidArgumentException;
use ArendBundles\SimpleDTOBundle\DTO\Exception\InvalidClassStringException;
use ArendBundles\SimpleDTOBundle\DTO\Exception\InvalidConstructorException;
use ArendBundles\SimpleDTOBundle\DTO\Exception\MissingInterfaceException;
use ReflectionClass;
use ReflectionException;
use RuntimeException;

/**
 * Class ArraySerializer.
 */
final class ArrayFormatSerializer implements FormatSerializerInterface, ArraySerializerInterface
{
    /**
     * @var PropertyFactoryInterface
     */
    private PropertyFactoryInterface $propertyFactory;

    /**
     * @var DTOFactoryInterface
     */
    private DTOFactoryInterface $defaultDTOFactory;

    /**
     * ArraySerializer constructor.
     *
     * @param PropertyFactoryInterface   $propertyFactory
     * @param DTOFactoryInterface $defaultDTOFactory
     */
    public function __construct(
        PropertyFactoryInterface $propertyFactory,
        DTOFactoryInterface $defaultDTOFactory
    ) {
        $this->propertyFactory = $propertyFactory;
        $this->defaultDTOFactory = $defaultDTOFactory;
    }

    /**
     * @param DTOInterface $dto
     *
     * @return array<string, mixed>
     */
    public function doSerialize(DTOInterface $dto): array
    {
        $resultSet = [];
        $reflection = new ReflectionClass($dto);

        foreach ($reflection->getProperties() as $reflectionProperty) {
            $property = $this->propertyFactory->create($reflectionProperty);
            $propertyName = $property->getPropertyName();
            if (!property_exists($dto, $propertyName)) {
                continue;
            }
            $resultSet[(string) $property->get(StringValue::NAME)] = $dto->$propertyName;
        }

        return $resultSet;
    }

    /**
     * @param DTOInterface|class-string $dto
     * @param array<string, mixed>      $data
     *
     * @return DTOInterface
     * @throws ClassIsAbstractException
     * @throws InvalidClassStringException
     * @throws InvalidConstructorException
     * @throws MissingInterfaceException
     */
    public function doDeserialize(DTOInterface|string $dto, $data): DTOInterface
    {
        if (!is_array($data)) {
            throw new InvalidArgumentException(sprintf('ArraySerializer expects data of type `array`. Given data is of type: `%s`', \gettype($data)));
        }

        if (!($dto instanceof DTOInterface)) {
            $dto = $this->defaultDTOFactory->create($dto);

        }

        try {
            $reflection = new ReflectionClass($dto);
        } catch (ReflectionException $exception) {
            throw new RuntimeException(
                sprintf('Unable to create reflection class from %s', \get_class($dto)),
                $exception->getCode(),
                $exception
            );
        }

        foreach ($reflection->getProperties() as $reflectionProperty) {
            $property = $this->propertyFactory->create($reflectionProperty);
            $propertyName = $property->getPropertyName();
            if (!property_exists($dto, $propertyName)) {
                continue;
            }
            $dto->$propertyName = $data[$property->get(StringValue::NAME)];
        }

        return $dto;
    }

    /**
     * @param DTOInterface $dto
     *
     * @return array<string, mixed>
     */
    public function toArray(DTOInterface $dto): array
    {
        return $this->doSerialize($dto);
    }

    /**
     * @param DTOInterface|class-string $dto
     * @param array<string, mixed>      $data
     *
     * @return DTOInterface
     * @throws ClassIsAbstractException
     * @throws InvalidClassStringException
     * @throws InvalidConstructorException
     * @throws MissingInterfaceException
     */
    public function fromArray(DTOInterface|string $dto, array $data): DTOInterface
    {
        return $this->doDeserialize($dto, $data);
    }
}