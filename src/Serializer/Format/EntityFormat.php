<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\Serializer\Format;

/**
 * Class EntityFormat
 */
final class EntityFormat extends AbstractFormat
{
    /**
     * EntityFormat constructor.
     *
     * @param FormatSerializerInterface $entityFormatSerializer
     */
    public function __construct(FormatSerializerInterface $entityFormatSerializer)
    {
        $this->setSerializer($entityFormatSerializer);
    }

    /**
     * @return string
     */
    public function getSerializerClass(): string
    {
        return EntityFormatSerializer::class;
    }
}