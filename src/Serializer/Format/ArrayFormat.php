<?php
declare(strict_types=1);
/**
 * @Author: Arend Hummeling
 */

namespace ArendBundles\SimpleDTOBundle\Serializer\Format;

/**
 * Class ArrayFormat.
 */
final class ArrayFormat extends AbstractFormat
{
    /**
     * ArrayFormat constructor.
     *
     * @param FormatSerializerInterface $serializer
     */
    public function __construct(FormatSerializerInterface $serializer)
    {
        $this->setSerializer($serializer);
    }

    /**
     * @return class-string
     */
    public function getSerializerClass(): string
    {
        return ArrayFormatSerializer::class;
    }
}