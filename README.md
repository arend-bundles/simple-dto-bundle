# Simple DTO Bundle
A work in progress to provide a simple complete bundle to provide the usage of data transfer objects. 

### Pipeline status
[![pipeline status](https://gitlab.com/arend-bundles/simple-dto-bundle/badges/master/pipeline.svg)](https://gitlab.com/arend-bundles/simple-dto-bundle/-/commits/master)

### Coverage
[![coverage report](https://gitlab.com/arend-bundles/simple-dto-bundle/badges/master/coverage.svg)](https://gitlab.com/arend-bundles/simple-dto-bundle/-/commits/master)


## What is a DTO?
DTO is an acronym for data transfer object. They are a design pattern used for the storage of any type of data, and enable generalized serialization, validation and object instantiation.
In a symfony project, you would use them as a dataclass for your formtypes, you'd implement them as a layer between formdata and the entity class. 

## Installation
First you must add the gitlab registry to your composer configuration, then you can `composer require` the bundle:
```shell
composer config repositories.gitlab.com/12000518 '{"type": "composer", "url": "https://gitlab.com/api/v4/group/12000518/-/packages/composer/packages.json"}'
composer req arend-bundles/simple-dto-bundle
```

## Usage
First you'd need a DTO class:
[Example](example/ExampleDTO.php)
(If you know of a way I can render the contents of this link inside the readme, please let me know. I don't mean hardcoding it in, I mean referencing the contents of the file)
Then you can inject a serializer in your class of choice to serialize the DTO, or deserialize serialized data into a DTO.


## Contribution

### Requirements
- docker

### Setup
I don't have composer installed (or php for that matter) locally, so you can use docker to run the setup.
If you have docker installed, you can run the following to install the vendor: <br>
```shell
./init.sh
```
This will build the build container and run the composer install with volume mounting.
After the installation, all the installed files will be owned by root, so sudo is used with chown to change ownership to the current user.
